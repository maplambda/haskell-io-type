# Haskell IO type
Brief notes on the IO type and its evaluation in Haskell

## IO type

i/o operations are executed by creating an action of ```IO``` type with a paramatised **value** to run. Until the action is run all values are 'suspended' awaiting evaluation.

An example is the ```putStrLn``` function which returns a type of ```IO``` with a paramatised value of ```Unit``` represented as ```()```. The operation of printing text to the console is suspended. The actual running of this type is handled by [GHC](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/index.html)/[GHCi](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/ghci.html)

```
:type putStrLn "Hello World!"
putStrLn "Hello World!" :: IO ()
```

In [GHCi](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/ghci.html) ```putStrLn``` will print "Hello World!" to the console, the result of running and printing the type.
``` 
putStrLn "Hello World!"
Hello World!
```

## Sequencing
Sequencing of operations is done with the **bind** operator ```>>=``` and a function returning an IO instance

```
putStrLn "Hello" >>= 
  (\x -> putStrLn "World!")

Hello
World!

:t putStrLn "Hello" >>= (\x -> putStrLn "World!") 
putStrLn "Hello" >>= (\x -> putStrLn "World!") :: IO ()
```

Haskell provides notation to combine any type providing the ```>>=``` operator

```
do
  putStrLn "Hello"
  putStrLn "World!"

Hello
World!
```

IO is a monad instance; ```pure``` or ```return``` can be used at the end of a **do** block to return a new ```IO``` action with an arbituary value, allowing a return type different to the last statement

```
do
  h <- pure "Hello"
  w <- pure "World!"
  return (h,w)

("Hello","World!")
```

The **value** of an ```IO``` action can be referenced from the argument ```a``` of the mapping function ```a -> IO b``` given to **bind** or using ```<-``` within **do**

```
:t getLine
getLine :: IO String

getLine >>= (\x -> putStrLn x)
> Hello World!
Hello World!

do
  x <- getLine
  putStrLn x
> Hello World!
Hello World!
```
## Execution

### Execution with GHC

The common execution method for an ```IO``` type is assigning it to the main function, compiling, and running the executable

```
cat << EOF > hello.hs
> main = putStrLn "hello"
> EOF

ghc -o hello hello.hs

./hello
Hello World!
```
### Unsafe execution

The 'back door entry' function ```unsafePerformIO``` can evaluate and return the value the ```IO``` type will run. **this is generally only used for testing**.

```
import System.IO.Unsafe

let hello = putStrLn "Hello World!"
unsafePerformIO hello

hello
()
```

### Edge of the world
The division between the interpreter running an ```IO``` context and the rest of the application built by composing ```IO``` is referred to as 'running at the edge of the world', the execution point of the entire application. 

## Composition
```mapM_```, ```foldM```, and other monad functions can be used to compose function application.

```
let hello = putStrLn "Hello World!"
mapM_ id $ take 4 $ repeat hello
Hello World!
Hello World!
Hello World!
Hello World!
```
*An alternative syntax for line 2 is: 
```mapM_ (\x->x) (take 4 (repeat hello))``` 
Use of **id** (the identity value for type) is considered [point free style](https://wiki.haskell.org/Pointfree) and the ```$``` operator [provides lower precedence function application ](https://hackage.haskell.org/package/basic-prelude-0.7.0/docs/CorePrelude.html#v:-36-) allowing the parameters to be omitted.*
```
let hello = putStrLn "Hello World!"
foldM (\_ a -> a) () $ take 4 $ repeat $ hello
Hello World!
Hello World!
Hello World!
Hello World!
```
## Effect systems

Haskell programming requires the monadic ```IO``` type due to lazy evaluation and pure functions (retaining the property of referential transparency while providing effects) but the ability to compose effects has been adopted for pure functional programming in languages such as Scala and Kotlin. — Scala: [Cats IO](https://typelevel.org/cats-effect/datatypes/io.html), [Scalaz Task](https://timperrett.com/2014/07/20/scalaz-task-the-missing-documentation/), [ZIO](https://scalaz.github.io/scalaz-zio/) 
— Kotlin: [Arrow IO](https://arrow-kt.io/docs/effects/io/)
